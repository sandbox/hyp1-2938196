Mobile Updater
================
Install this module and grant users that are managing 
the mobile application version 
"Access Mobile Updater configuration" permission.

Go to admin/config/mobile_update to configure 
your app version and download url for your mobile app.
Client applications can check for the current version 
at {base_url}/app/update/{version}.
The version string format should be like #.#.#

If the client app version is lower than the version on site, the module will return 
a version string of 'false' and an update url as JSON object.
ex:
http://{base_url}/app/update/0.0.9
{'version:false','update_url':'http://download.com/mobile'}  
The client should now use the update url to update the application 

If the version is equal or higher than the version on site, it will return
the current version and the update url as JSON object.
ex:
http://{base_url}/app/update/1.0.0
{'version:1.0.0','update_url':'http://download.com/mobile'}  

A testcase "MobileUpdateDrupalWebTestCase" for this module is included.